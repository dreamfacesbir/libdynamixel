#ifndef DYNAMIXEL_SERVOS_HPP_
#define DYNAMIXEL_SERVOS_HPP_

#include "servos/ax12.hpp"
#include "servos/ax12w.hpp"
#include "servos/ax18.hpp"
#include "servos/ex106.hpp"
#include "servos/mx12.hpp"
#include "servos/mx28.hpp"
#include "servos/mx28_p2.hpp"
#include "servos/mx64.hpp"
#include "servos/mx64_p2.hpp"
#include "servos/mx106.hpp"
#include "servos/mx106_p2.hpp"

#endif
