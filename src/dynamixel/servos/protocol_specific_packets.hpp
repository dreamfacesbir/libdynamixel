#ifndef DYNAMIXEL_SERVOS_PROTOSPECIFICPACKETS_HPP_
#define DYNAMIXEL_SERVOS_PROTOSPECIFICPACKETS_HPP_

#include <cmath>
#include <vector>

#include "../errors/error.hpp"
#include "../errors/servo_limit_error.hpp"
#include "../instruction_packet.hpp"
#include "../instructions/reboot.hpp"
#include "../protocols.hpp"
#include "base_servo.hpp"
#include "model_traits.hpp"

namespace dynamixel {
    namespace servos {

        //template <class Model>
        //class Servo;

        template <class M, class P>
        class BaseProtocolSpecificPackets {
        public:
            typedef typename ModelTraits<M>::CT ct_t;

            /** Build a packet to set the desired speed on an actuator.

                See the protocol-specific implemetnations for details.

                @param id identifier of the actuator
                @param rad_per_s rotational speed, in radians per second
                @param operating_mode (enum) mode in which the actuator is
                    controlled; for version 1 of the protocol, it has an impact
                    on how the values are treated
                @return a data packet to be sent on the serial line
            **/
            static inline InstructionPacket<P> set_moving_speed_angle(
                typename P::id_t id,
                double rad_per_s,
                OperatingMode operating_mode = OperatingMode::joint)
            {
                throw errors::Error("set_moving_speed_angle not implemented for this protocol");
            }

            /** Build a packet to set the desired speed on an actuator (in register).

                See the protocol-specific implemetnations for details.

                @param id identifier of the actuator
                @param rad_per_s rotational speed, in radians per second
                @param operating_mode (enum) mode in which the actuator is
                    controlled; for version 1 of the protocol, it has an impact
                    on how the values are treated
                @return a data packet to be sent on the serial line
            **/
            static inline InstructionPacket<P> reg_moving_speed_angle(
                typename P::id_t id,
                double rad_per_s,
                OperatingMode operating_mode = OperatingMode::joint)
            {
                throw errors::Error("reg_moving_speed_angle not implemented for this protocol");
            }

            static inline InstructionPacket<P> clear_errors(typename P::id_t id)
            {
                throw errors::Error("clear_errors not implemented for this protocol");
            }

            static inline InstructionPacket<P> clear_errors(typename P::id_t id, long long int value)
            {
                throw errors::Error("clear_errors not implemented for this protocol");
            }

            /** Build a packet to read the present state (position, speed, load) of an actuator

                See the protocol-specific implementations for details.

                @param id identifier of the actuator
                @return a data packet to be sent on the serial line
            **/
            static inline InstructionPacket<P> get_present_state(typename P::id_t id)
            {
                throw errors::Error("get_present_state not implemented for this protocol");
            }

            static inline void parse_present_state(typename P::id_t id, const StatusPacket<P>& st, double * state)
            {
                throw errors::Error("parse_present_state not implemented for this protocol");
            }

            /** Build a packet to set the desired position and speed on an actuator (in register).

                See the protocol-specific implemetnations for details.

                @param id identifier of the actuator
                @param rad_per_s rotational speed, in radians per second
                @param operating_mode (enum) mode in which the actuator is
                    controlled; for version 1 of the protocol, it has an impact
                    on how the values are treated
                @return a data packet to be sent on the serial line
            **/
            static inline InstructionPacket<P> reg_position_and_speed_angle(
                typename P::id_t id,
                double rad,
                double rad_per_s)
            {
                throw errors::Error("reg_position_and_speed_angle not implemented for this protocol");
            }
        };

        template <class M, class P>
        class ProtocolSpecificPackets: public BaseProtocolSpecificPackets<M, P> {};

        template <class M>
        class ProtocolSpecificPackets<M, protocols::Protocol1>: public BaseProtocolSpecificPackets<M, protocols::Protocol1> {
        public:
            typedef typename ModelTraits<M>::CT ct_t;
            typedef typename ct_t::goal_position_t goal_position_t;
            typedef typename ct_t::moving_speed_t moving_speed_t;
            typedef typename ct_t::torque_limit_t torque_limit_t;

            static inline InstructionPacket<protocols::Protocol1> set_moving_speed_angle(
                typename protocols::Protocol1::id_t id,
                double rad_per_s,
                OperatingMode operating_mode)
            {
                moving_speed_t speed_ticks = angular_speed_to_ticks(id, rad_per_s,
                    operating_mode);

                return M::set_moving_speed(id, speed_ticks);
            }

            static inline InstructionPacket<protocols::Protocol1> reg_moving_speed_angle(
                typename protocols::Protocol1::id_t id,
                double rad_per_s,
                OperatingMode operating_mode)
            {
                moving_speed_t speed_ticks = angular_speed_to_ticks(id, rad_per_s,
                    operating_mode);

                return M::reg_moving_speed(id, speed_ticks);
            }

            static inline InstructionPacket<protocols::Protocol1> clear_errors(typename protocols::Protocol1::id_t id)
            {
                torque_limit_t limit = ct_t::max_torque_limit;
                return M::set_torque_limit(id, limit);
            }

            static inline InstructionPacket<protocols::Protocol1> clear_errors(typename protocols::Protocol1::id_t id, long long int value)
            {
                torque_limit_t limit = static_cast<torque_limit_t>(value);
                return M::set_torque_limit(id, limit);
            }

            static inline InstructionPacket<protocols::Protocol1> get_present_state(typename protocols::Protocol1::id_t id)
            {
                typename protocols::Protocol1::address_t address = ct_t::present_position;
                typename protocols::Protocol1::length_t length =
                    sizeof(typename ct_t::present_position_t) + sizeof(typename ct_t::present_speed_t) + sizeof(typename ct_t::present_load_t);

                return instructions::Read<protocols::Protocol1>(id, address, length);
            }

            static inline void parse_present_state(typename protocols::Protocol1::id_t id, const StatusPacket<protocols::Protocol1>& st, double * state)
            {
                std::vector<uint8_t> packet = st.parameters();

                if (packet.size() != 6)
                    throw errors::UnpackError(1, packet.size(), 6);

                uint16_t raw_pos = (((uint16_t)packet[1]) << 8) | ((uint16_t)packet[0]);
                uint16_t raw_speed = (((uint16_t)packet[3]) << 8) | ((uint16_t)packet[2]);
                uint16_t raw_load = (((uint16_t)packet[5]) << 8) | ((uint16_t)packet[4]);

                // position
                double deg = ((raw_pos - ct_t::min_goal_position) * (ct_t::max_goal_angle_deg - ct_t::min_goal_angle_deg) / (ct_t::max_goal_position - ct_t::min_goal_position)) + ct_t::min_goal_angle_deg;
                double rad = deg / 57.2958;

                // speed
                int8_t sign;
                if (ct_t::speed_sign_bit) { // the highest bit is used for the sign
                    sign = raw_speed / ct_t::max_goal_speed == 0 ? 1 : -1;
                    raw_speed = raw_speed % ct_t::max_goal_speed;
                }
                else {
                    sign = 1;
                }
                double speed_rpm = sign * raw_speed * ct_t::rpm_per_tick;
                double speed_si = speed_rpm * 0.104720; // convertion ratio is 2*pi/60

                // load
                double load = (raw_load & 0x3FF) / 1024.0;  // bits 0-9 are data, bit 10 is sign, expressed as ratio of max torque 0-1
                if (raw_load & 0x400)
                    load = -load;

                state[0] = rad; state[1] = speed_si; state[2] = load;
            }

            static inline InstructionPacket<protocols::Protocol1> reg_position_and_speed_angle(
                typename protocols::Protocol1::id_t id,
                double rad,
                double rad_per_s)
            {
                double deg = rad * 57.2958;
                if (!(deg >= ct_t::min_goal_angle_deg && deg <= ct_t::max_goal_angle_deg))
                    throw errors::ServoLimitError(
                        id,
                        ct_t::min_goal_angle_deg * 0.01745,
                        ct_t::max_goal_angle_deg * 0.01745,
                        rad);
                goal_position_t pos = ((deg - ct_t::min_goal_angle_deg) * (ct_t::max_goal_position - ct_t::min_goal_position) / (ct_t::max_goal_angle_deg - ct_t::min_goal_angle_deg)) + ct_t::min_goal_position;

                moving_speed_t speed_ticks = angular_speed_to_ticks(id, rad_per_s, OperatingMode::joint);

                typename protocols::Protocol1::address_t address = ct_t::goal_position;

                std::vector<uint8_t> packed_data(4);
                packed_data[0] = (uint8_t)(pos & 0xFF);
                packed_data[1] = (uint8_t)((pos >> 8) & 0xFF);
                packed_data[2] = (uint8_t)(speed_ticks & 0xFF);
                packed_data[3] = (uint8_t)((speed_ticks >> 8) & 0xFF);

                return instructions::RegWrite<protocols::Protocol1>(id, address, packed_data);
            }

        private:
            // 2 * pi
            static constexpr double two_pi = 6.28318;

            static inline moving_speed_t angular_speed_to_ticks(
                typename protocols::Protocol1::id_t id,
                double rad_per_s,
                OperatingMode operating_mode)
            {
                // convert radians per second to ticks
                int32_t speed_ticks = round(60 * rad_per_s / (two_pi * ct_t::rpm_per_tick));

                // The actuator is operated as a wheel (continuous rotation)
                if (operating_mode == OperatingMode::wheel) {
                    // Check that desired speed is within the actuator's bounds
                    if (!(abs(speed_ticks) >= ct_t::min_goal_speed && abs(speed_ticks) <= ct_t::max_goal_speed)) {
                        double min_goal_speed = -ct_t::max_goal_speed * ct_t::rpm_per_tick * two_pi / 60;
                        double max_goal_speed = ct_t::max_goal_speed * ct_t::rpm_per_tick * two_pi / 60;
                        throw errors::ServoLimitError(id, min_goal_speed, max_goal_speed, rad_per_s, "speed (rad/s)");
                    }

                    // Move negatives values in the range [ct_t::min_goal_speed,
                    // ct_t::2*max_goal_speed+1]
                    if (speed_ticks < 0) {
                        speed_ticks = -speed_ticks + ct_t::max_goal_speed + 1;
                    }
                }
                // The actuator is operated as a joint (not continuous rotation)
                else if (operating_mode == OperatingMode::joint) {
                    if (!(speed_ticks >= ct_t::min_goal_speed && speed_ticks <= ct_t::max_goal_speed)) {
                        double min_goal_speed = ct_t::min_goal_speed * ct_t::rpm_per_tick * two_pi / 60;
                        double max_goal_speed = ct_t::max_goal_speed * ct_t::rpm_per_tick * two_pi / 60;
                        throw errors::ServoLimitError(id, min_goal_speed, max_goal_speed, rad_per_s, "speed (rad/s)");
                    }
                }

                return (moving_speed_t)speed_ticks;
            }
        };

        template <class M>
        class ProtocolSpecificPackets<M, protocols::Protocol2>: public BaseProtocolSpecificPackets<M, protocols::Protocol2> {
        public:
            using BaseProtocolSpecificPackets<M, protocols::Protocol2>::clear_errors;
            typedef typename ModelTraits<M>::CT ct_t;
            typedef typename ct_t::moving_speed_t moving_speed_t;

            static inline InstructionPacket<protocols::Protocol2> set_moving_speed_angle(
                typename protocols::Protocol2::id_t id,
                double rad_per_s,
                OperatingMode operating_mode)
            {
                moving_speed_t speed_ticks = angular_speed_to_ticks(id, rad_per_s);

                return M::set_moving_speed(id, speed_ticks);
            }

            static inline InstructionPacket<protocols::Protocol2> reg_moving_speed_angle(
                typename protocols::Protocol2::id_t id,
                double rad_per_s,
                OperatingMode operating_mode)
            {
                moving_speed_t speed_ticks = angular_speed_to_ticks(id, rad_per_s);

                return M::reg_moving_speed(id, speed_ticks);
            }

            static inline InstructionPacket<protocols::Protocol2> clear_errors(typename protocols::Protocol2::id_t id)
            {
                return instructions::Reboot<protocols::Protocol2>(id);
            }

            static inline InstructionPacket<protocols::Protocol2> get_present_state(typename protocols::Protocol2::id_t id)
            {
                typename protocols::Protocol2::address_t address = ct_t::present_current;
                typename protocols::Protocol2::length_t length =
                    sizeof(typename ct_t::present_current_t) + sizeof(typename ct_t::present_speed_t) + sizeof(typename ct_t::present_position_t);

                return instructions::Read<protocols::Protocol2>(id, address, length);
            }

        private:
            // 2 * pi
            static constexpr double two_pi = 6.28318;

            static inline moving_speed_t angular_speed_to_ticks(
                typename protocols::Protocol2::id_t id,
                double rad_per_s)
            {
                // convert radians per second to ticks
                int32_t speed_ticks = round(60 * rad_per_s / (two_pi * ct_t::rpm_per_tick));

                // Check that desired speed is within the actuator's bounds
                if (!(abs(speed_ticks) <= ct_t::max_goal_speed)) {
                    double min_goal_speed = ct_t::min_goal_speed * ct_t::rpm_per_tick * two_pi / 60;
                    double max_goal_speed = ct_t::max_goal_speed * ct_t::rpm_per_tick * two_pi / 60;
                    throw errors::ServoLimitError(id, min_goal_speed, max_goal_speed, rad_per_s, "speed (rad/s)");
                }

                return (moving_speed_t)speed_ticks;
            }
        };
    } // namespace servos
} // namespace dynamixel

#endif // DYNAMIXEL_SERVOS_PROTOSPECIFICPACKETS_HPP_
