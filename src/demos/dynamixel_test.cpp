#include <iostream>
#include <cmath>

#include <dynamixel/dynamixel.hpp>

using namespace dynamixel;
using namespace controllers;
using namespace servos;
using namespace instructions;
using namespace protocols;

int main(int argc, char** argv)
{
    if (argc != 3) {
        std::cout << "Usage: " << argv[0] << " device" << " 0:1" << std::endl;
        return -1;
    }

    try {
#ifndef __APPLE__
        Usb2Dynamixel controller(argv[1], B1000000, 0.01);
#else
        Usb2Dynamixel controller(argv[1], B115200, 0.01);
#endif
        std::vector<std::shared_ptr<BaseServo<Protocol1>>> servos = auto_detect<Protocol1>(controller);
        StatusPacket<Protocol1> st;
        //  dynamixel::InstructionPacket<Protocol1> inst0;
        // dynamixel::InstructionPacket<Protocol1> inst1;

        for (auto servo : servos) {
            std::cout << "Detected an " << servo->model_name() << " with ID " << servo->id() << std::endl;
            // controller.send(servo->set_torque_enable(1));
            // controller.recv(st);
            // controller.send(servo->set_moving_speed_angle(1));
            // controller.recv(st);
            // controller.send(servo->set_goal_position_angle(M_PI));
            // controller.recv(st);
            // try{

            // std::vector<InstructionPacket<Protocol1>> ip0;
            // // ip0.push_back(servo->set_status_return_level(1));
            // ip0.push_back(servo->set_led(0));
            // ip0.push_back(servo->set_torque_enable(0));
            // // ip0.push_back(servo->set_status_return_level(2));

            // std::vector<InstructionPacket<Protocol1>> ip1;
            // // ip1.push_back(servo->set_status_return_level(1));
            // ip1.push_back(servo->set_led(1));
            // ip1.push_back(servo->set_torque_enable(1));
            // // ip1.push_back(servo->set_status_return_level(2));

            // controller.send(servo->set_torque_limit(1023));
            // controller.recv(st);
            // servo->clear_errors(1023);
            // controller.send(servo->set_goal_position(10));
            // controller.recv(st);
            controller.send(servo->clear_errors(1023));
            
            // for(size_t i = 0; i < 10; ++i){
            //     // controller.send(inst0);
            //     // controller.recv(st);
            //     controller.send(ip0);
            //     //controller.recv(st);
            //     usleep(1000000);
            //     // controller.send(inst1);
            //     // controller.recv(st);
            //     controller.send(ip1);
            //     //controller.recv(st);
            //     usleep(1000000);
            // }

            // controller.send(servo->set_status_return_level(2));
            // controller.recv(st);
            // }
            // catch (dynamixel::errors::Error& e) {
            //     std::cout << "Caught exception: " << e.msg() << std::endl;
            // }
        }
    }
    catch (const errors::Error& e) {
        std::cerr << "error (dynamixel): " << e.msg() << std::endl;
    }

    return 0;
}
